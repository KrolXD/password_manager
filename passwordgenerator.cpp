#include "passwordgenerator.h"
#include "ui_passwordgenerator.h"
#include <QClipboard>

PasswordGenerator::PasswordGenerator(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordGenerator)
{
    ui->setupUi(this);
}


PasswordGenerator::~PasswordGenerator()
{
    delete ui;
}


void PasswordGenerator::on_horizontalSlider_valueChanged(int value)
{
    ui->label_passwordLenghtValue->setText(QString::number(value));
    makePassword();
}


void PasswordGenerator::makePassword()
{
    int lenght = ui->label_passwordLenghtValue->text().toInt();
    bool upper, lower, numbers, symbols;
    QString selectedCharacters, lowerCharacters, upperCharacters, numbersCharacters, symbolsCharacters;
    QString password;
    lowerCharacters = "qwertyuiopasdfghjklzxcvbnm";
    upperCharacters = "QWERTYUIOPASDFGHJKLZXCVBNM";
    numbersCharacters = "0123456789";
    symbolsCharacters = "!@#$%^&*";
    upper = ui->checkBox_uppercase->isChecked();
    lower = ui->checkBox_lowercase->isChecked();
    numbers = ui->checkBox_numbers->isChecked();
    symbols = ui->checkBox_symbols->isChecked();
    if(upper)
    {
        selectedCharacters.append(upperCharacters);
    }
    if(lower)
    {
        selectedCharacters.append(lowerCharacters);
    }
    if(numbers)
    {
        selectedCharacters.append(numbersCharacters);
    }
    if(symbols)
    {
        selectedCharacters.append(symbolsCharacters);
    }
    if(upper||lower||numbers||symbols)
    {
        for(int i=0; i<lenght; ++i)
        {
            int index = qrand() % selectedCharacters.length();
            QChar nextChar = selectedCharacters.at(index);
            password.append(nextChar);
        }
        ui->lineEdit_password->setText(password);
    }
}


void PasswordGenerator::on_checkBox_uppercase_stateChanged(int arg1)
{
    makePassword();
}


void PasswordGenerator::on_checkBox_lowercase_stateChanged(int arg1)
{
    makePassword();
}


void PasswordGenerator::on_checkBox_numbers_stateChanged(int arg1)
{
    makePassword();
}


void PasswordGenerator::on_checkBox_symbols_stateChanged(int arg1)
{
    makePassword();
}


void PasswordGenerator::on_pushButton_copy_clicked()
{
    QString password = ui->lineEdit_password->text();
    QClipboard *cb = QApplication::clipboard();
    cb->setText(password);
}


void PasswordGenerator::on_pushButton_refresh_clicked()
{
    makePassword();
}
