#ifndef PASSWORDGENERATOR_H
#define PASSWORDGENERATOR_H

#include <QDialog>

namespace Ui {
class PasswordGenerator;
}

class PasswordGenerator : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordGenerator(QWidget *parent = nullptr);
    ~PasswordGenerator();
    void makePassword();

private slots:
    void on_horizontalSlider_valueChanged(int value);

    void on_checkBox_uppercase_stateChanged(int arg1);

    void on_checkBox_lowercase_stateChanged(int arg1);

    void on_checkBox_numbers_stateChanged(int arg1);

    void on_checkBox_symbols_stateChanged(int arg1);

    void on_pushButton_copy_clicked();

    void on_pushButton_refresh_clicked();

private:
    Ui::PasswordGenerator *ui;
};

#endif // PASSWORDGENERATOR_H
