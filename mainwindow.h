#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void addLoginData(QString name, QString login, QString password);
    void editRow(QLineEdit* name, QLineEdit* login, QLineEdit* password);

private slots:
    void on_actionadd_triggered();

    void on_action_password_generator_triggered();

    void on_actionShow_all_paswords_triggered();

    void on_actionHide_all_passwords_triggered();

    void on_actionRemove_all_triggered();

    void on_actionby_name_triggered();

    void on_actionby_login_triggered();

    void on_actionAuthor_triggered();

    void on_actionApplication_triggered();

    void on_actionsave_triggered();

    void on_actionopen_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
