#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addloginpassword.h"
#include "passwordgenerator.h"
#include <QLabel>
#include <QLineEdit>
#include <QLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QCoreApplication>
#include <QClipboard>
#include <QStringList>
#include <QList>
#include <QFileDialog>
#include "simplecrypt.h"
#include <QTextStream>
#include <QInputDialog>
#include <QCryptographicHash>
#include <QChar>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionadd_triggered()
{
    QString name,login,password;
    AddLoginPassword dialogALP;
    dialogALP.setModal(true);
    dialogALP.exec();
    if(dialogALP.getOk())
    {
        name = dialogALP.getName();
        login = dialogALP.getLogin();
        password = dialogALP.getPassword();
        addLoginData(name,login,password);
    }
}


void MainWindow::on_action_password_generator_triggered()
{
    PasswordGenerator dialogPG;
    dialogPG.setModal(true);
    dialogPG.exec();
}


void MainWindow::addLoginData(QString name, QString login, QString password)
{

    QLabel *lable_name = new QLabel(this);
    QLabel *lable_login = new QLabel(this);
    QLabel *lable_password = new QLabel(this);
    lable_name->setText("Name:");
    lable_login->setText("Login:");
    lable_password->setText("Password");


    QLineEdit *lineEdit_name = new QLineEdit(this);
    QLineEdit *lineEdit_login = new QLineEdit(this);
    QLineEdit *lineEdit_password = new QLineEdit(this);
    lineEdit_name->setText(name);
    lineEdit_login->setText(login);
    lineEdit_password->setText(password);
    lineEdit_name->setReadOnly(1);
    lineEdit_login->setReadOnly(1);
    lineEdit_password->setReadOnly(1);
    lineEdit_password->setEchoMode(QLineEdit::Password);


    QPushButton* btn_loginCopy = new QPushButton(this);
    QPushButton* btn_passwordCopy = new QPushButton(this);
    QPushButton* btn_passwordShow = new QPushButton(this);
    QPushButton* btn_rowEdit = new QPushButton(this);
    QPushButton* btn_rowDelete = new QPushButton(this);


    btn_loginCopy->setIcon(QIcon(":/icons/copy.png"));
    btn_passwordCopy->setIcon(QIcon(":/icons/copy.png"));
    btn_passwordShow->setIcon(QIcon(":/icons/visibility.png"));
    btn_rowEdit->setIcon(QIcon(":/icons/pencil.png"));
    btn_rowDelete->setIcon(QIcon(":/icons/delete (1).png"));


    QHBoxLayout *row = new QHBoxLayout(this);
    row->addWidget(lable_name);
    row->addWidget(lineEdit_name);
    row->addWidget(lable_login);
    row->addWidget(lineEdit_login);
    row->addWidget(btn_loginCopy);
    row->addWidget(lable_password);
    row->addWidget(lineEdit_password);
    row->addWidget(btn_passwordCopy);
    row->addWidget(btn_passwordShow);
    row->addWidget(btn_rowEdit);
    row->addWidget(btn_rowDelete);


    ui->verticalLayout_data->addLayout(row);


    connect(btn_loginCopy, &QPushButton::clicked,
                   [=]() {
        QClipboard *cb = QApplication::clipboard();
        cb->setText(lineEdit_login->text());
    });

    connect(btn_passwordCopy, &QPushButton::clicked,
                   [=]() {
        QClipboard *cb = QApplication::clipboard();
        cb->setText(lineEdit_password->text());
    });

    connect(btn_passwordCopy, &QPushButton::clicked,
                   [=]() {
        QClipboard *cb = QApplication::clipboard();
        cb->setText(lineEdit_password->text());
    });

    connect(btn_passwordShow, &QPushButton::clicked,
                   [=]() {
        if(lineEdit_password->echoMode() == QLineEdit::Password)
        {
            lineEdit_password->setEchoMode(QLineEdit::Normal);
            btn_passwordShow->setIcon(QIcon(":/icons/invisible.png"));
        }
        else
        {
            lineEdit_password->setEchoMode(QLineEdit::Password);
            btn_passwordShow->setIcon(QIcon(":/icons/visibility.png"));
        }
    });

    connect(btn_rowEdit, &QPushButton::clicked,
                   [=]() {
        editRow(lineEdit_name,lineEdit_login,lineEdit_password);
    });

    connect(btn_rowDelete, &QPushButton::clicked,
                   [=]() {
        delete lable_name;
        delete lineEdit_name;
        delete lable_login;
        delete lineEdit_login;
        delete btn_loginCopy;
        delete lable_password;
        delete lineEdit_password;
        delete btn_passwordCopy;
        delete btn_passwordShow;
        delete btn_rowEdit;
        delete btn_rowDelete;
        delete row;
    });
}


void MainWindow::on_actionShow_all_paswords_triggered()
{
    for (int i = 0; i < ui->verticalLayout_data->count(); ++i)
    {
      QLayout *row = ui->verticalLayout_data->itemAt(i)->layout();
      if (row != NULL)
      {
            QWidget *widget = row->itemAt(6)->widget();
            QLineEdit *password = dynamic_cast<QLineEdit*>(widget);
            password->setEchoMode(QLineEdit::Normal);
      }
    }
}


void MainWindow::on_actionHide_all_passwords_triggered()
{
    for (int i = 0; i < ui->verticalLayout_data->count(); ++i)
    {
        QLayout *row = ui->verticalLayout_data->itemAt(i)->layout();
        if (row != NULL)
        {
            QWidget *widget = row->itemAt(6)->widget();
            QLineEdit *password =dynamic_cast<QLineEdit*>(widget);
            password->setEchoMode(QLineEdit::Password);
        }
    }
}


void MainWindow::on_actionRemove_all_triggered()
{
    int rowsCount = ui->verticalLayout_data->count();
    for (int i = 0; i < rowsCount; ++i)
    {
        QLayout *row = ui->verticalLayout_data->itemAt(0)->layout();
        int itemsInRow = row->count();
        for(int j = 0; j < itemsInRow; j++)
        {
            QWidget *widget = row->itemAt(0)->widget();
            delete widget;
        }
        delete row;
    }
}


void MainWindow::editRow(QLineEdit *name, QLineEdit *login, QLineEdit *password)
{
    QString strName,strLogin,strPassword;
    strName = name->text();
    strLogin = login->text();
    strPassword = password->text();


    AddLoginPassword dialogALP;
    dialogALP.setModal(true);
    dialogALP.setName(strName);
    dialogALP.setLogin(strLogin);
    dialogALP.setPassword(strPassword);
    dialogALP.exec();


    if(dialogALP.getOk())
    {
        name->setText(dialogALP.getName());
        login->setText(dialogALP.getLogin());
        password->setText(dialogALP.getPassword());
    }

}


void MainWindow::on_actionby_name_triggered()
{
    QStringList names,logins,passwords, sortednames;
    int rowsCount = ui->verticalLayout_data->count();


    for (int i = 0; i < rowsCount; ++i)
    {
        QLayout *row = ui->verticalLayout_data->itemAt(0)->layout();
        int itemsInRow = row->count();

        for(int j = 0; j < itemsInRow; j++)
        {
            QWidget *widget = row->itemAt(0)->widget();
            if(j == 1)
            {
            QLineEdit *name =dynamic_cast<QLineEdit*>(widget);
            names.append(name->text());
            }
            if(j == 3)
            {
                QLineEdit *login =dynamic_cast<QLineEdit*>(widget);
                logins.append(login->text());
            }
            if(j == 6)
            {
                QLineEdit *password =dynamic_cast<QLineEdit*>(widget);
                passwords.append(password->text());
            }
            delete widget;
        }
        delete row;
    }


    sortednames = names;
    sortednames.removeDuplicates();
    sortednames.sort();


    for(int i = 0; i < sortednames.size(); i++)
    {
        for(int j = 0; j < names.size(); j++)
        {
            if(sortednames.at(i) == names.at(j))
            {
                addLoginData(names.at(j), logins.at(j), passwords.at(j));
            }
        }
    }
}


void MainWindow::on_actionby_login_triggered()
{
    QStringList names,logins,passwords, sortedlogins;
    int rowsCount = ui->verticalLayout_data->count();


    for (int i = 0; i < rowsCount; ++i)
    {
        QLayout *row = ui->verticalLayout_data->itemAt(0)->layout();
        int itemsInRow = row->count();

        for(int j = 0; j < itemsInRow; j++)
        {
            QWidget *widget = row->itemAt(0)->widget();
            if(j == 1)
            {
            QLineEdit *name =dynamic_cast<QLineEdit*>(widget);
            names.append(name->text());
            }
            if(j == 3)
            {
                QLineEdit *login =dynamic_cast<QLineEdit*>(widget);
                logins.append(login->text());
            }
            if(j == 6)
            {
                QLineEdit *password =dynamic_cast<QLineEdit*>(widget);
                passwords.append(password->text());
            }
            delete widget;
        }
        delete row;
    }


    sortedlogins = logins;
    sortedlogins.removeDuplicates();
    sortedlogins.sort();


    for(int i = 0; i < sortedlogins.size(); i++)
    {
        for(int j = 0; j < logins.size(); j++)
        {
            if(sortedlogins.at(i) == logins.at(j))
            {
                addLoginData(names.at(j), logins.at(j), passwords.at(j));
            }
        }
    }
}


void MainWindow::on_actionAuthor_triggered()
{
    QMessageBox messageBoxAuthor;
    messageBoxAuthor.setText("Author: Karol Chowaniec student of computer science at the Uniwersytet Śląski \n"
                             "Contact: karolchowaniecc@gmail.com");
    messageBoxAuthor.exec();
}


void MainWindow::on_actionApplication_triggered()
{
    QMessageBox messageBoxApp;
    messageBoxApp.setText("The application is used to store and create passwords \n for applications and websites");
    messageBoxApp.exec();
}

void MainWindow::on_actionsave_triggered()
{
    bool ok;
    QString filePassword = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                             tr("Set file password:"), QLineEdit::Normal,
                                             "password", &ok);
    if (ok && !filePassword.isEmpty())
    {
        QString hashPassword = QString(QCryptographicHash::hash((filePassword.toUtf8()),QCryptographicHash::Md5).toHex());
        long long int filePasswordValue=1;
        for(int i = 0; i<filePassword.length(); i++)
        {
            QChar ch = filePassword.at(i);
            filePasswordValue *= ch.toLatin1();
        }


        QString fileName = QFileDialog::getSaveFileName(this,
                tr("Save logins and passwords"), "",
                tr("login data (*.txt)"));
        if (fileName.isEmpty())
        {
            return;
        }
        else
        {
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly))
            {
                QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
                return;
            }
            QTextStream out(&file);
            out << hashPassword << "\n";

            int rowsCount = ui->verticalLayout_data->count();


            for (int i = 0; i < rowsCount; ++i)
            {
                QLayout *row = ui->verticalLayout_data->itemAt(i)->layout();

                QWidget *widgetName = row->itemAt(1)->widget();
                QWidget *widgetLogin = row->itemAt(3)->widget();
                QWidget *widgetPassword = row->itemAt(6)->widget();


                QLineEdit *name =dynamic_cast<QLineEdit*>(widgetName);
                QLineEdit *login =dynamic_cast<QLineEdit*>(widgetLogin);
                QLineEdit *password =dynamic_cast<QLineEdit*>(widgetPassword);
                QString strToSave = name->text() + ";" + login->text() + ";" + password->text();

                SimpleCrypt crypto(filePasswordValue);
                QString cryptoResult = crypto.encryptToString(strToSave);
                out << cryptoResult << "\n";
            }
            file.close();
        }
    }
}

void MainWindow::on_actionopen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Choose login data file"), "",
            tr("login data (*.txt)"));
    if (fileName.isEmpty())
    {
        return;
    }
    else
    {
        bool ok;
        QString filePassword = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                                 tr("Write file password:"), QLineEdit::Normal,
                                                 "password", &ok);
        if (ok && !filePassword.isEmpty())
        {
            QString hashPassword = QString(QCryptographicHash::hash((filePassword.toUtf8()),QCryptographicHash::Md5).toHex());
            QFile file(fileName);
            if (!file.open(QIODevice::ReadOnly))
            {
                QMessageBox::information(this, tr("Unable to open file"),
                file.errorString());
                return;
            }
            on_actionRemove_all_triggered();
            QTextStream in(&file);
            QString passwordLine = in.readLine();
            if(hashPassword == passwordLine)
            {
                long long int filePasswordValue=1;
                for(int i = 0; i<filePassword.length(); i++)
                {
                    QChar ch = filePassword.at(i);
                    filePasswordValue *= ch.toLatin1();
                }


                SimpleCrypt crypto(filePasswordValue);
                while(!in.atEnd())
                {
                    QString line = in.readLine();
                    QString decrypted = crypto.decryptToString(line);
                    QStringList splited = decrypted.split( ";" );
                    addLoginData(splited.at(0), splited.at(1), splited.at(2));
                }
            }
            file.close();
        }
    }
}
