QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addloginpassword.cpp \
    main.cpp \
    mainwindow.cpp \
    passwordgenerator.cpp \
    simplecrypt.cpp

HEADERS += \
    addloginpassword.h \
    mainwindow.h \
    passwordgenerator.h \
    simplecrypt.h

FORMS += \
    addloginpassword.ui \
    mainwindow.ui \
    passwordgenerator.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    icons.qrc

DISTFILES += \
    icons/copy.png \
    icons/delete (1).png \
    icons/invisible.png \
    icons/pencil.png \
    icons/refresh.png \
    icons/visibility.png
