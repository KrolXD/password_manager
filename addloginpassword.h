#ifndef ADDLOGINPASSWORD_H
#define ADDLOGINPASSWORD_H

#include <QDialog>

namespace Ui {
class AddLoginPassword;
}

class AddLoginPassword : public QDialog
{
    Q_OBJECT

public:
    explicit AddLoginPassword(QWidget *parent = nullptr);
    ~AddLoginPassword();
    void makePassword();
    QString getName();
    QString getLogin();
    QString getPassword();
    void setName(QString name);
    void setLogin(QString login);
    void setPassword(QString password);
    bool getOk();

private slots:
    void on_horizontalSlider_valueChanged(int value);

    void on_checkBox_uppercase_stateChanged(int arg1);

    void on_checkBox_lowercase_stateChanged(int arg1);

    void on_checkBox_numbers_stateChanged(int arg1);

    void on_checkBox_symbols_stateChanged(int arg1);

    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::AddLoginPassword *ui;
};

#endif // ADDLOGINPASSWORD_H
