#include "addloginpassword.h"
#include "ui_addloginpassword.h"
#include "mainwindow.h"

AddLoginPassword::AddLoginPassword(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddLoginPassword)
{
    ui->setupUi(this);

}


AddLoginPassword::~AddLoginPassword()
{
    delete ui;
}


void AddLoginPassword::on_horizontalSlider_valueChanged(int value)
{
    ui->label_passwordLenghtValue->setText(QString::number(value));
    makePassword();
}


void AddLoginPassword::makePassword()
{
    int lenght = ui->label_passwordLenghtValue->text().toInt();
    bool upper, lower, numbers, symbols;
    QString selectedCharacters, lowerCharacters, upperCharacters, numbersCharacters, symbolsCharacters;
    QString password;
    lowerCharacters = "qwertyuiopasdfghjklzxcvbnm";
    upperCharacters = "QWERTYUIOPASDFGHJKLZXCVBNM";
    numbersCharacters = "0123456789";
    symbolsCharacters = "!@#$%^&*";
    upper = ui->checkBox_uppercase->isChecked();
    lower = ui->checkBox_lowercase->isChecked();
    numbers = ui->checkBox_numbers->isChecked();
    symbols = ui->checkBox_symbols->isChecked();
    if(upper)
    {
        selectedCharacters.append(upperCharacters);
    }
    if(lower)
    {
        selectedCharacters.append(lowerCharacters);
    }
    if(numbers)
    {
        selectedCharacters.append(numbersCharacters);
    }
    if(symbols)
    {
        selectedCharacters.append(symbolsCharacters);
    }
    if(upper||lower||numbers||symbols)
    {
        for(int i=0; i<lenght; ++i)
        {
            int index = qrand() % selectedCharacters.length();
            QChar nextChar = selectedCharacters.at(index);
            password.append(nextChar);
        }
        ui->lineEdit_password->setText(password);
    }
}


void AddLoginPassword::on_checkBox_uppercase_stateChanged(int arg1)
{
    makePassword();
}


void AddLoginPassword::on_checkBox_lowercase_stateChanged(int arg1)
{
    makePassword();
}


void AddLoginPassword::on_checkBox_numbers_stateChanged(int arg1)
{
    makePassword();
}


void AddLoginPassword::on_checkBox_symbols_stateChanged(int arg1)
{
    makePassword();
}


void AddLoginPassword::on_pushButton_ok_clicked()
{
    ui->label_ok->setText("0");
    close();
}


void AddLoginPassword::on_pushButton_cancel_clicked()
{
    close();
}


QString AddLoginPassword::getName()
{
    QString name = ui->lineEdit_name->text();
    return name;
}


QString AddLoginPassword::getLogin()
{
    QString login = ui->lineEdit_login->text();
    return login;
}


QString AddLoginPassword::getPassword()
{
    QString password = ui->lineEdit_password->text();
    return password;
}


bool AddLoginPassword::getOk()
{
    QString ok = ui->label_ok->text();
    if(ok == "0")
    {
        return true;
    }
    else
    {
        return false;
    }
}


void AddLoginPassword::setName(QString name)
{
    ui->lineEdit_name->setText(name);
}


void AddLoginPassword::setLogin(QString login)
{
    ui->lineEdit_login->setText(login);
}


void AddLoginPassword::setPassword(QString password)
{
    ui->lineEdit_password->setText(password);
}
